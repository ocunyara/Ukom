var gulp = require('gulp'),
    less = require('gulp-less'),
    rigger = require('gulp-rigger'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload,
    autoprefix = require('gulp-autoprefixer');

var src = {
    lessAll: 'app/src/styles/**/*.less',
    less: 'app/src/styles/style.less',
    html: 'app/src/*.html',
    htmlAll: 'app/src/**/*.html',
    image: 'app/src/image/*',
    font: 'app/src/font/*',
    scripts: 'app/src/scripts/*',
};

var dist = {
    css:  'app/compiled/styles/',
    html: 'app/compiled/',
    image: 'app/compiled/image/',
    font: 'app/compiled/font/',
    scripts: 'app/compiled/scripts/',
};

gulp.task('css', function () {
    gulp.src(src.less)
        .pipe(less())
        .pipe(autoprefix('last 3 version'))
        .pipe(gulp.dest(dist.css))
        .pipe(reload({stream: true}));
});

gulp.task('html:build', function () {
    gulp.src(src.html) 
        .pipe(rigger())
        .pipe(gulp.dest(dist.html))
        .pipe(reload({stream: true}));
});

gulp.task('font:build', function() {
    gulp.src(src.font)
        .pipe(gulp.dest(dist.font))
});

gulp.task('image:build', function() {
    gulp.src(src.image)
        .pipe(gulp.dest(dist.image))
});
gulp.task('script:build', function() {
    gulp.src(src.scripts)
        .pipe(gulp.dest(dist.scripts))
});


gulp.task('serve', ['css'], function() {

    browserSync({
        server: dist.html
    });

    gulp.watch(src.lessAll, ['css']);
    gulp.watch(src.htmlAll,['html:build']).on('change', reload);

});


gulp.task('default', ['serve','css','html:build',
    'font:build','image:build','script:build'], function () {});